<?php
namespace KDA\Packages\LocalisationManager;
use KDA\Laravel\PackageServiceProvider;
//use Illuminate\Support\Facades\Blade;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasCommands;
    /* This is mandatory to avoid problem with the package path */
    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }
    //-------------------------------------------
    /*public function register()
    {
        parent::register();
    }*/
    /**
     * called after the trait were registered
     */
    public function postRegister(){
    }
    //called after the trait were booted
    protected function bootSelf(){
    }
}
